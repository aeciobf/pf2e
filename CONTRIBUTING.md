If you would like to contribute for the FoundryVTT Pathfinder 2e System localization into the Brazilian Portuguese language, you can do so in one of the following ways:

1. Sending merge requests to this repository with the pt-BR.json file with its translated strings.

2. Creating new issues in this repository containing any new translation or error that you found.
